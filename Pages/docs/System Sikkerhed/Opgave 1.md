opgave 1:
Fagets læringsmål

instruktioner:

1. Læs og refleketer over læringsmålene individuelt
Tids estimate: 15 minutter
Hvert team medlem skal individuelt læse og reflekter over studieordningens læringsmål for faget System sikkerhed.
2. Noter individuelt et konkret eksempel på hvert læringsmål.
Tids estimate: 15 minutter
Hvert team medlem noter individuelt et konkret eksempel som taler ind til hvert læringsmål. F.eks. læringsmålet Sikkerhedadministration i Database management system -> Credentials for hver database
Som 1.Semester studerende kan dette være meget svært, men der er ikke noget rigtig og forkert, blot hvad du allerede ved, eller kan søge dig til
3. lav en fælles forståelse af læringsmålene.
Tids estimate: 30 minutter
Benyt CL strukturen Møde på midten til at udarbejde en fælles formulering af konkrette eksempler som taler ind til hvert læringsmål(gerne flere konkrete eksempler for hver læringsmål).
Punkt 1 & 2 i møde på midten strukturen blev opfyldt i forrige opgave
4. Efter pausen Noter læringsmål med konkret eksemepl i padlet.
Tids estimate: 10 minutter
Teamet skal nu skrive læringsmålene og de konkrette eksempler ind i den fælles padlet.

Udførsel:
1. læst of udført. link til Læringsmål: https://esdhweb.ucl.dk/D22-1980440.pdf?_ga=2.115552518.1057771612.1675433091-1226966722.1675433091
2. Viden:
Den studerende har viden om:

- Generelle Governance Principper / sikkerhedsprocedurer (implementering af sikkerhed i systemet ved at overholde love)
- Væsentlige forensic processer (hvis et system har været hacket skal man kunne finde ud af hvordan det er sket osv.)
- Relevante it trusler (hvordan bliver folk for det meste hacket)
- Relevante sikkerhedsprincipper til systemsikkerhed (hvordan gør man et system mere sikkert)'
- OS roller ift. sikkerhedsovervejelser (authorization)
- sikkerhedsadministration i DBMS (credentials til databaser)

Færdigheder
den studerende kan:

- udnytte modforanstaltinger til sikring af systemer (sikring imod kendte trusler)
- følge et benchmark til at sikre opsætning af enhederne (følge en opskrift?)
- Implementere systematisk logning og moitorering af enheder (overvågning af systemer)
- analysere logs for incidents og flge et revisionsspor (forensics, har nogen været inde og lavet noget de ikke må)
- kan genoprette et system efter en hændelse (backups, med minmial tab af data)

Kompetencer
Den studerende kan:
- Håndtere enheder på commandline niveau (lave noget fra terminal hvis der ikke er en UI)
- Håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler (firewalls???)
- Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at forhindre, detektere og reagere overfor specifikke it-sikkerhedsmæssige hændelser (have nok viden om systemsikkerhed til at kunne udvælge det bedste hardware og software til et specifikt system)
- håndtere relevante krypteringstiltag (hashing/salting, kryptering indenfor systemer)



3. Udført: https://docs.google.com/document/d/15xhyv-jrjXVA2j-yr9orn1zWpDqS-Tt6YAJ2r6hB3JE/edit
4. udført: https://padlet.com/eyk9jtwttn/system-sikkerheds-l-ringm-l-ea6tgilxdkhe44u9 kode: P?t4`n0w3.&IhewVIAFSOi|\

