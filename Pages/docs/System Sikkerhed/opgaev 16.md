opgave 16, linux log lukning

information:
Logging daemons såsom rsyslog kan lukkes ned, på lige fod med andre applikations processer i Linux. Dette skal udføres i denne opgave

instruktioner:
1. Eksekver kommandoen service rsyslog stop Efter dette vil der ikke længere blive genereret logs i operativ systemet.
2. Eksekver kommandoen service rsyslog start

Typisk er den kun superbrugerer som kan lukke ned for en log service. Det betyder at hvis en angriber kan lukke ned for logging servicen, kan han også lukke ned for evt. sikkerheds mekanismer som er i samme operativ system. Overvej hvordan man kan undgå dette? med F.eks. log overvågning via netværk.

udførsel:
1. ![img_15.png](img_15.png)
2. ![img_16.png](img_16.png)