opgave 12, linux log system

instruktioner:

primærer log file

Den primær log file for systemet hedder syslog, og indeholder information om
stort alt systemet foretager sig.

1. udskriv inholdet af denne file.
2. studer log formattet, og skriv et "generisk" format ind i dit Linux cheat sheet.
3. Noter hvilken tids zone loggen bruger til tidsstemple, er det UTC?

Authentication log

Log filen auth.log indeholder information om autentificering.

1. udskriv indholdet af auth.log
2. skrift bruger til F.eks. root
3. udskriv indholdet af auth.log igen, og bemærk de nye rækker
4. skrift tilbage til din primær bruger igen (som naturligvis ikke er root)
5. udskriv indholdet af auth.log igen, og bemærk de nye rækker

Udførsel:

primære log file:

1. ![img_4.png](img_4.png)
2. de første tre bogstaver i måneden, dd, HH-MM-SS, bruger, system management daemon?, besked
3. tidsformatet er UTC AM/PM 12 timers format.

Authentication log:
1. ![img_5.png](img_5.png)
2. udført
3. ![img_6.png](img_6.png)
4. udført
5. ![img_7.png](img_7.png)