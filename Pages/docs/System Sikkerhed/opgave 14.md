opagev 14, logging regler for rsyslog kan ændres

information:
Reglerne for hvad rsyslog skal logge, findes i filen 50-default.conf.

Reglerne for skrives op som facility.priority action

Facility er programmet der bliver logget fra F.eks. mail eller kern. Priority fortæller system hvilket types besked der skal logges. Beskeder typer identificerer hvilken priortering den enkelte log har. Nedenunder er der en besked typer listet i priorteret rækkefølge. Højeste priortet først.

1. emerg. 
2. alert. 
3. crit. 
4. err. 
5. warning. 
6. notice. 
7. info. 
8. debug.

Altså log beskeder af typen emerg er vigtige beskeder der bør reageres på med det samme. Hvor imod debug beskeder er mindre vigtige. Priority kan udskiftes med wildcard *, hvilket betyder at alle besked typer skal sendes til den file som er defineret i action

instruktioner
1. i ryslog directory, skal filen 50-default.conf findes.
2. Åben filen 50-deault.conf
3. Dan et overblik over alle de log filer som der bliver sendt beskeder til.
4. Noter hvilken filer mail applikationen sender log beskeder til, ved priorteringen info , warn og err

udførsel:
1. ![img_10.png](img_10.png)
2. udført
3. ![img_11.png](img_11.png)
4. ![img_12.png](img_12.png)
