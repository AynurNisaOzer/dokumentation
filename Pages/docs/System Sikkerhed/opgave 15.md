opgave 15, log rotation i linux

Information

Når der løbende bliver indsamlet logs, vil log filerne typisk kunne blive meget store henover tid, og optage meget plads på lagrings mediet. For at undgå dette kan man roterer logs i et givet tidsrum. F.eks. kan en log roteres hver 3. Uge. Hvis log filen auth.log roteres vil, den skifte navn til auth.log.1 og en ny file ved navn auth.log vil blive oprettet og modtag alle nye logs.

auth.log.1 bliver en såkaldt backlog file.

instruktioner:

I disse opgaver skal der arbejdes med log rotation i Linux (Ubuntu).

Du kan finde hjælp i Ubuntu man page til logrotate.

1. åben filen /etc/logrotate.conf
2. Sæt log rotationen til daglig rotation
3. sæt antallet af backlogs til 8 uger.
Yderlige kan du prøve at sætte logrotationen op så den følger CFCS vejledning.

udførsel:
1. udført
2. ![img_13.png](img_13.png)
3. ![img_14.png](img_14.png)