opgave 17, linux log overblik

Information

I denne opgave er den nogle spørgsmål som kan hjælpe dig med at samle dit overblik over log systemet i Linux. Nogle af spørgsmålene referer til rsyslog, mens andre referer til Journalctl log systemet. Du vil formodentlig komme ud for at skulle søge svaret online, hvilket er en del af opgaven.

Instruktioner

1. Hvad hedder den rsyslog file som indeholder alle begivenheder som relaterer til autentificering?
2. Hvilken log file indeholder oplysninger om bruger der er logget ind på nuværende tidspunkt?
3. Hvilken log system eksisterer på næsten alle moderne Linux systemer?

udførsel:
1. auth.log
2. user.log
3. syslog daemon