opgave 26, installer audit daemon

instruktion
1. Ubuntu har som udgangspunkt ikke audit daemon installeret. Installer audit daemon med kommandoen apt install auditd
2. Verificer at auditd er aktiv med kommandoen systemctl status auditd
3. brug værktøjet auditctl til at udskrive nuværende regler med kommandoen auditctl -l
4. Udskriv log filen som findes i /var/log/audit/audit.log


udførsel
1. Udført
2. ![img_19.png](img_19.png)
3. man skal være root for at køre kommandoen. ![img_20.png](img_20.png)
4. ![img_21.png](img_21.png)

