opgave 3, system sikkerhed

introduktion af grundlæggende linux kommandoer

instruktioner:
1. eksekver kommandoen pwd
2. eksekver kommandoen cd .. 
3. eksekver kommandoen cd / 
4. Lokationen / har en bestemt betydning i Linux file systemet. Hvad er det? (Her kan du med fordel søge svar med Google)
5. eksekver kommandoen cd /etc/ca-certificates/ 
6. Hvor mange Directories viser outputtet fra pwd kommandoen nu? 
7. eksekver kommandoen cd ../.. 
8. Hvor mange Directories viser outputtet fra pwd kommandoen nu? 
9. eksekver kommandoen cd ~ (Karakteren hedder tilde. I Ubuntu kan den nogen gange findes med knappen F6)
10. Kommandoen ~ er en "Genvej" i linux, hvad er det en genvej til? 
11. i file systemets rod(/), esekver kommandoen ls 
12. i brugerens Home directory, eksekver kommandoen touch helloworld.txt 
13. i brugerens Home directory, eksekver kommandoen touch .helloworld.txt 
14. list alle filer og mapper i brugerens Home directory 
15. list alle filer og mapper i brugerens Home directory med flaget -a 
16. list alle filer og mapper i brugerens Home directory med flaget -l 
17. list alle filer og mapper i brugerens Home directory med flaget -la 
18. i brugerens Home directory, eksekver kommandoen mkdir helloWorld 
19. Eksekver kommandoen ls -d */ 
20. Eksekver kommandoenn ls -f

21. Test dig selv
Udfør den følgende opgave, med så lidt hjælp som muligt
Opret directoriet minefiler i Home directory. i minefiler skal der oprettes en file ved navn min fil1, og en skjult file ved navn skjultfil1. Udfør herefter følgende:

1. Udskriv en liste til konsolen som viser alle ikke skjulte filer. 
2. Udskriv en liste til konsolen som viser alle filer (Inklusiv skjulte filer)
3. Gå tilbage til Home directory. 
4. Lav et skjult directory ved navn skjult (hvordan symboliseret vi tidligere skjulte filer?)

udførsel:
1. så vidt jeg kunne se, gjorde denne kommando at jeg kunne se hvilket directory jeg er i
2. denne kommando gjorde at jeg gik en directory/mappe tilbage
3. denne kommando gjorde at jeg kom i root directory
4. root directory
5. kommandoen gjorde at jeg kom i et andet directory
6. 2
7. kom tilbage til mit home directory
8. viser kun root (/)
9. udført
10. denote til brugerens home directory
11. udfrt
12. udført gav ikke lov
13. udført gav ikke lov
14. 1 (aynur)
15. 3 (., .., aynur)
16. 4 ()
17. 12
18. udført
19. udført

