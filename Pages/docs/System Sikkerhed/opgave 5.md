opgave 5, systemsikkerhed

grundlæggende linux kommandoer

intruktioner:
1. Eksekver kommandoen apt --help. 
2. via apt --help skal du nu finde ud af hvordan du udskriver en liste til konsolen, som viser alle installeret pakker. 
3. Eksekver kommandoen apt -h. 
4. Eksekver kommandoen man apt og scroll ned i bunden af konsol outputtet(pgdwn)
5. Eksekver kommandoen man ls og scroll ned i bunden af konsol outputtet 
6. Eksekver kommandoen man man og skim outputtet, hvilken information kan du finde der? 
7. eksekver kommandoen help

udførsel:
1. package manager, viser/hjælper med common kommandoer
2. apt list
3. viser det samme som apt --h
4. viser manual for apt
5. viser manual for ls
6. manual for manual
7. vise en liste over kommandoer man kan bruge/indtaste