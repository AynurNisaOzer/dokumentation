opgave 9, brugersystemer i linux

instruktioner:
1. I denne opgave skal der oprettes en ny bruger ved navn Mandalorian.
2. I denne opgave skal brugeren Mandalorian, havde tildelt et password.
3. I denne opgave skal Mandalorian bruger kontoen ændres således den har en udløbs dato 23-06-2023.
4. I denne opgave skal bruger kontoen Mandalorian havde ændret sit hjemme directory til /home/mandoFiles/. husk at oprette directoriet først.
5. I Linux er alle filer ejet af en bestemt bruger. Typisk når en bruger opretter en file, får denne bruger også tildelt ejerskab over filen. 
Med kommandoen ls <filenavn> -al kan du få vist en tilladelse for en specifik file. Som vist på billede nedenunder:
Bruger rettigheder på en file
Formatet er som vist nedenunder
|Ejerens rettigheder|-|Gruppens rettigheder|-|Alle andres rettigheder|Antal links til filen| Ejer af filen| tilknyttet gruppe | Forklaring på resten af kolonerne er bevidst undladt indtil videre

Hvad enten man er på rød eller blå hold, er det nyttigt at kunne finde de filer som er tilknytet en specifik konto. Dette kan man gøre med kommandoen find sammen med flaget -r. flaget user kan også bruges

I denne opgave skal følgende trin udføres:

a. opret tre filer med brugeren Mandalorian.

b. find dem med find kommandoen, eksekver søgning fra /.

Bemærk de mange "Permission denied" og den lange søge tid

6. I denne opgave skal brugeren Mandalorian slettes, udfør opgaven som følgende.

- slet brugeren Mandalorian med userdel.
- Find en af de filer som blevet oprettet af brugeren Mandalorian
- Se rettighederne for denne file, og noter hvem der er file ejer.
- Opret en bruger ved navn Ivan.
- Gentag trin 3(Samme file)
- Vurder om dette er en potetienelle sårbarhed, og om man bør overeje altid at slette/skifte ejerskab på filer når en bruger slettes
- Et alternativ til at slette bruger kontoer, er at deaktiver dem
- Overvej om princippet Secure by default reelt er overholdt

7. I linux er der som udgangspunkt to filer som er intressant ift. bruger systemet. Den ene er filen passwd. I de fleste distributioner kan denne findes i stien /etc/passwd. passwd indeholder alle informationer om bruger kontoer(undtaget passwords).

I denne opgave skal du udforske passwd filen.

- Se rettighederne for passwd med kommandoen ls <filenavn> -al.
- Overvej om rettighederne som udgangspunkt ser hensigtmæssige ud? og hvorfor der er de rettigheder som der er.
- Udskriv filens indhold Hvis du har glemt hvordan så kig i dit cheat sheet. øvelsen blev udført i opgave 7
- Samhold filens indhold med nedstående format (BEMÆRK: ikke alle kolonner er nødvendigvis tilstede)
- Brugernavn:Password(legacy, ikke brugt længere derfor x istedet):Bruger id:Gruppe id:Bruger information:Hjemme directory:Default shell

8. I linux er der som udgangspunkt to filer som er intressant ift. bruger systemet. Den ene er filen passwd. I de fleste distributioner kan denne findes i stien /etc/passwd. passwd indeholder alle informationer om bruger kontoer(undtaget passwords).

I denne opgave skal du udforske passwd filen.

- Se rettighederne for passwd med kommandoen ls <filenavn> -al.
- Overvej om rettighederne som udgangspunkt ser hensigtmæssige ud? og hvorfor der er de rettigheder som der er.
- Udskriv filens indhold Hvis du har glemt hvordan så kig i dit cheat sheet. øvelsen blev udført i opgave 7
- Samhold filens indhold med nedstående format (BEMÆRK: ikke alle kolonner er nødvendigvis tilstede)
- Brugernavn:Password(legacy, ikke brugt længere derfor x istedet):Bruger id:Gruppe id:Bruger information:Hjemme directory:Default shell

9. Filen shadow opbevare passwords til alle bruger kontoer. Alle passwords i shadow er hashes, som sikring mod udvekommende adgang til bruger kontoer.

I opgaven skal du udforske filen shadow.

- udskriver rettighederne for filen shadow
- Overvej rettighederne i samhold med Privilege of least princippet.
- Udskriv filens indhold.
- Samhold filens indhold med nedstående format.
:brugernavn:password(hashet):Sidste ændring af password:Minimum dage ind password skift:Maksimum dage ind password skift:Varslings tid for udløbet password:Antal dage med udløbet password inden dekativering af konto:konto udløbs dato
Nogen af jer er endnu ikke introduceret til password hashes, hvilket er okay. I skal blot noter i jeres cheatsheet at i kan finde en forklaring på hashes i shadow filen i denne opgave.

Følgende viser et eksempel på et hash i shadow filen. $y$j9T$GfMsEAQ8t9EkjiOiDzVRA0$cWqq3SrEZED3EvJYMFD/G1TYn8lgWOaSM8IvjCeD4j2:19417
formattet er som følger:
$hash algoritme id$salt$has$
Hash algoritmernes id kan man slå op. I eksemplet er y brugt. Det vil sige at hash algoritmen til at generer hashen er yescrypt.

10. Tiltrods for at yescrypt som udgangspunkt er en stærk algoritme. Så yder hashet kun begrænset beskyttelse til svage kodeord. Dette skal der eksperimenteres med i denne opgave. I de følgende trin skal der oprettes en bruger med et svagt kodeord. Herefter skal vi trække kodeord hashet ud fra shadow filen, og forsøg genskabe passwordet ud fra hash værdien.

Alle kommandoer skal eksekveres mens du er i dit hjemme directory
1. installer værktøjer john-the-ripper med kommandoen sudo apt install john
2. Her efter skal du downloade en wordlist kaldet rockyou med følgende kommando wget https://github.com/brannondorsey/naive-hashcat/releases/download/data/rockyou.txt.
3. Opret nu en bruger ved navn misternaiv og giv ham passwordet password
4. Hent nu det hashet kodeord for misternaiv med følgende kommando sudo cat /etc/shadow | grep misternaiv > passwordhash.txt
5. udskriv indholdet af filen passwordhash.txt, og bemærk hvilken krypterings algoritme der er brugt.
6. Eksekver nu kommandoen john -wordlist=rockyou.txt passwordhash.txt.
7. Kommandoen resulter i No password loaded. Dette er fordi john the ripper værktøjet ikke selv har kunne detekter hvilken algoritme det drejer sig om.
8. Eksekver nu kommandoen: john --format=crypt -wordlist=rockyou.txt passwordhash.txt.
format fortæller john the ripper hvilken type algoritme det drejer sig om
9. Resultat skulle gerne være at du nu kan se kodeordet, som vist på billede nedenunder.
password crack

1. gentag processen fra trin 1 af. Men med et stærkt password.(minnimum 16 karakterer, både store og små bogstaver, samt special tegn)
2. Reflekter over hvorfor komplekse kodeord er vigtige (Og andre gange en hæmmesko for sikkerheden).

Udførsel:
1. ![img.png](img.png)
2. ![img_1.png](img_1.png)
3. ![img_2.png](img_2.png)
4. ![img_3.png](img_3.png)
5. 


Ressourcer:
https://linux.die.net/man/8/useradd
