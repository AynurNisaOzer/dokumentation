ipam opgave, netværk og kommunikationssikkerhed

instruktioner:
1. papir+blyant øvelse.
Lav en IP subnet oversigt over dine devices og de tilhørende interne og eksterne netværk.
Anvend meget gerne det netværk i påtænker til semester projektet!
2. Research hvad netbox er, lav en liste over hvad det kan og hvad det ikke kan.

3. Set netbox VM op og dokumentér dit netværk via netbox interfacet. Brug getting started fra netbox dokumentationen (som er lavet med mkdocs!!)

Netbox credentials:  
consolen viser ip adressen
netbox:netbox som password til web interface.
andre logins i console 

a. Hvis i allerede har netværket til semester projektet skitseret eller tegnet som netværksdiagram så anvend det

b. Alternativt kig på netværket i den virksomhed du er ansat i

c. Ellers søg på nettet efter large network diagram eller large network topology
4. Hvis i bruger en virksomhed i opgaven så se på det IPAM dokumentation som virksomheden allerede har og forhold dig til det.
Alternativt research hvilke andre måder i kan lave IPAM dokumentation på. Noter det som en liste i jeres dokumentation.

udførsel:
1. https://docs.google.com/document/d/1D6k5WI1L1ztSA6FlJElunpQIzKOY8-WvCl7_Aj6Tb1c/edit
2. netbox er:
- single source of truth
- API (web)
2. netbox er ikke:
- Netværk skanner
3. https://192.168.91.130/