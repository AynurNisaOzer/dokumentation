dahboard opgave, netværk pg kommunikationssikkerhed

instruktioner:

testing:
- Download a test vm from here and install it in vmware workstation
- Connect it to vmnet111 (or the equivante 192.168.111.0/24 subnet)
- There is an information page on http://192.168.111.20

Working with Grafana:

- Make a block diagram showing the relationship between node_exporter, prometheus and grafana.
- Add a grafana dashboard that uses the node_exporter values from prometheus
- Add a grafana dashboard that uses the logs from loki
- Set up an extra VM with node_exporter and update prometheus to scrape from it


Udførsel:

Testing = udført.

Working with Grafana:
- 

