opgave 1, netværk og kommunikationssikkerhed

1. finde læringsmålene for faget
2. diskutere i teams, hvordan disse er relevante

udførsel:

1. https://esdhweb.ucl.dk/D22-1980440.pdf?_ga=2.106729795.1332599022.1675671412-199463755.1631293529

2. Hvordan er læringsmålene relevante?
- vidensdelen er grundelementer/teknologier inden for faget.
- færdighedsdelen er relevante pga. at sikkerhedsdelen af faget. altså det giver mening at vi kender relevante sårbarheder og kan identificere dem.
- kompetence delen er relevant fordi, det giver mening at man har så meget viden om et sikkert netværk, at man kan opbygge et fra bunden af, samt finde huller og forbedre et der ikke er sikkert.
