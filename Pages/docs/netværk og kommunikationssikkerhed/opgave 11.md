opgave 11, Basic DNS

Instruktioner:
- Gennemfør rummet https://tryhackme.com/room/dnsindetail
- Skriv dine opdagelser ned undervejs og dokumenter det på gitlab

- Besvar herefter følgende om DNS og dokumenter undervejs:

1. Forklar med dine egne ord hvad DNS er og hvad det bruges til.
2. Hvilke dele af CIA er aktuelle og hvordan kan DNS sikres? https://blog.cloudflare.com/dns-encryption-explained/
3. Hvilken port benytter DNS ?
4. Beskriv DNS domæne hierakiet
5. Forklar disse DNS records: A, AAAA, MX, TXT og CNAME.
6. Brug nslookup til at undersøge hvor mange mailservere ucl.dk har
7. Brug dig til at finde txt records for ucl.dk
8. Brug wireshark til:
9. Filtrere DNS trafik. Dokumenter hvilket filter du skal bruge for kun at se DNS trafik
10. Lav screenshots af eksempler på DNS trafik

Udførsel:
- udført
- for at finde de forskellige ting:
![img_6.png](img_6.png)
![img_7.png](img_7.png)
![img_8.png](img_8.png)
![img_9.png](img_9.png)


1. DNS er et system, som husker IP-adresser for dig. så istedet for et IP-adresse for en hjemmeside skal man kun huske URL'en på hjemmesiden
2. transort laget er aktuelt, og dns kan sikres ved kryptering
3. port 53
4. 