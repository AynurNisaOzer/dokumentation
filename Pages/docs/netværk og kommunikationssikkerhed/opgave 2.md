opgave 2, netværk og kommunikationssikkerhed

finde hardware og placere dem på OSI-modellen

instrukser:
1. Find almindelige (consumer) eksempler på hardware enheder, og placer dem på OSI modellen.
2. Find nye/sjældne/esoteriske enheder (fysiske eller virtuelle) (med referencer) - f.eks. next-gen firewalls, proxies, application gateways. Hvad-som-helst med et netstik/wifi er ok at have med.
3. Forbered jer til diskussion/præsentation af hvad i har fundet så vi kan dele viden på klassen.

udførsel:
1. det fysiske lag: kabler, Wireless (Bluetooth, Airdrop), Fiberkabler,
2. data link laget: netværkskort, layer 2 switches
3. netværks laget: Routere, layer 3 switches
4. transport laget: Gateways, Firewalls
5. Sessionlaget: Gateways, Firewalls, PC´er
6. præsentationslaget: Gateways, Firewalls, PC´er
7. Applikationslaget: end devices såsom PC´er, smartphones og Servere

Ressourcer:
Links om OSI modellen se: 
- https://moozer.gitlab.io/course-networking-basisc/02_OSI/osi_model/
- https://www.comparitech.com/net-admin/osi-model-explained/ 
- https://www.youtube.com/watch?v=vv4y_uOneC0


opsummering af tilegnede erfaringer: