Opgave 3, øvelse i fejlfinding

Instruktioner:

1. Vælg et problem, gerne når det opstår, alternativt på bagkant. Det kunne være printerfejl, wifi connection issues, hjemmesider der er ‘væk’, langsomt internet, certifikat fejl, mv 
2. Beskriv problemet. Vær opmærksom på forskellen mellem hvad der er observeret, og hvad der er "rygmarvsreaktion". 
3. Beskriv hvad du tror problemet er (ie. formuler en hypotese)
4. Find oplysninger der underbygger/afkræfter ideen (ie. se i log filer, lav tests, check lysdioder, …) og skriv det ned 
5. Hvis ideen blev afkræftet, lav en ny hypotese. 
6. Beslut hvad der kan gøres for at afhjælpe eller mitigere fejlen, hvis den skulle komme igen. 
7. Implementer det hvis ressourcerne tillader det, eller beskriv hvordan det skal håndteres til næste gang. 
8. Bonus spørgsmål: Hvordan håndterede du at nå grænsen for din viden og/eller forståelse af problemet eller et del-element?

Udførsel:

1. Jeg kunne ikke pushe kode til gitlab. 
2. hver gang jeg havde fået lavet noget inde på projektet og committed det gik det igennem. Men ligesåsnart jeg prøvede at pushe disse commits, fik jeg fejlen "du kan ikke pushe kode til dette repo"
3. jeg tror at jeg fik lavet projektet, eller downloaded den på en forkert måde
4. jeg slettede projektet fra min pc, og slettede den inde fra gitlab og lavede en ny. jeg var opmærksom på alle instruktioner i forbindelse med oprettelse og downloading af den med SSH.
5. idéen blev afkræftet da jeg stadig ikke kunne committe kode.
ny hypotese er at der var noget galt med min forbindelse til gitlab ie. SSH

Jeg var ikke sikker på hvordan jeg skulle undersøge denne hypotese, så jeg gik ind på nettet og prøvede at google mig frem. Da jeg ikke kunne finde noget brugbart
sendte jeg en mail til min lære og prøvede at høre ham om det. Han gjorde mig opmærksom på at det så ud som om at repoet var lavet med en gitlab konto/ssh forbindelse, mens jeg prøvede at committe med et andet
da jeg blev opmærksom på det gik jeg ind på gitlab, og inviterede mit andet konto til projektet. Efter det kunne jeg sagtens pushe kode til repoet.

Der kan ikke gøres så meget for at hjælpe med at forhindre det samme problem i at opstå, ud over at man skal være opmærksom på de kontoer/ssh´er man har


ressourcer:
- stackoverflow
- beskeder til nikolaj

Opsummering af tilegnede erfaringer:

Jeg har lært at man skal være opmærksom på de kontoer man har, og laver ting med.