opgave 37 - DNS

Information:

Vi har tidligere snakket om problemer med ARP og spoofing. Se f.eks. opgaverne med ettercap

Det er også med i MITRE att&ck framework.


Instrukser:

1. DNS er meget brugt i angreb
- DNS og C2
- recon
- attacker DNS server
- compromised DNS
- Passive DNS
Der er flere. Søg selv efter DNS.

2. Er det noget der bliver brugt af angriberne?

3. Hvad er der er mitigeringer?

4. Hvad er der af detection?

5. Hvordan kan suricata og ssl inspection bruges i denne kontekst?

Udførsel:
2. DNS er noget der bliver brugt af angribere. tit vil de bruge det, for at gemme sig blandt alle de andre DNS trafikker. fx:
![img_11.png](img_11.png)


3. Af mitigeringer er der:
![img_10.png](img_10.png)

4. Af detection er der:
![img_12.png](img_12.png)

5. 
Ressourcer:
