opgave 9, netværk og kommunikationssikkerhed

Instruktioner:
Find to-tre protokoller fra wiresharks test pakker som du kan genkende

Forstå hvad der foregår, evt. vha. google

Tag noter, og vis det næste gang

Udførsel:

1. HTTP: ![img_1.png](img_1.png)
først et trevejshåndtryk med TCP protokollen. derefter bliver der sagt at der skal hentes en fil med en HTTP protokol der hedder GET..

2. TCP: ![img_2.png](img_2.png)
frst trevejs håndtryk. psh ack giver besked om at den har modtaget alle dataer og fin ack siger at hosten har termineret/vil terminere connection.

3. telnet: ![img_3.png](img_3.png)
først foregår der en trevejs håndtryk. derefter er der nogen dataer der bliver sendt frem og tilbag.


Resourcer:
