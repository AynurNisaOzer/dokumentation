Beskrivelse af opgaven:

Opgave 8 del 9

Oprettelse af gitignore samt pipeline

Udførelse af opgaven:

Først starter man ud med at oprette en fil i roden af projektet. Det kalder man for. Gitignore. Herefter åbner man filen og tilføjer linjerne venv/ og site/, venv kan også være env/ men på min hedder det venv. Derefter opretter man en fil til pipeline og kalder den for. Gitlab-ci.yml. Det er vigtigt at kalde filen for præcis for det. Så tilføjer man de linjer man skal have i dem, i det korrekte syntaks. Til sidst laver man en git commit i en terminal og pusher den. Til sidst checker man sin pipeline inde på gitlab fra en browser, og hvis alting går som det skal burde den være "passed", ellers skal man tjekke hvad man har gjort forkert.

Problemer jeg er stødt på under udførslen:

I løbet af opgave 8 havde jeg nogle problemer:
1. jeg kunne ikke pushe til min gitlab repo
2. pipeline blev ved med at fejle

løsninger:
1. Det viste sig at mens jeg havde oprettet projektet med mit gitlab konto med mit ucl-mail samt skole ssh key, så viste det sig at jeg rettede på projektet fra min Computer med min egen gitlab-konto og ssh key. derfor inviterede jeg min private gitlab-konto som en owner til projektet, og jeg havde derefter ikke problemer med at pushe til repo´et længere. 
2. Her viste det sig at syntaksen først var forkert. da jeg havde fået rettet op på det ville det ikke virke igen, pga. jeg havde to brugere, og den bruger som jeg arbejdede fra, altså den som jeg ikke havde oprettet projektet fra skulle verificeres, for at jeg kunne køre en pipeline. man kan verificere ved at gå ind på sin gitlab konto fra en browser og under pipelines kommer der er en pop-besked frem hvor der står at man skal verificeres.


ressourcer:
- stackoverflow
- beskeder til nikolaj samt Magnus
- https://gitlab.com/npes/mkdocs-test

Opsummering af tilegnede erfaringer:

Jeg har lært at når man bruger to forskellige gitlab kontoer på den samme repo skal man sørge for at, de begge har adgang med rettigheder samt at de er verificerede.